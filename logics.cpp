#include "logics.h"

Logics::Matrix::Matrix()
{
    for (size_t i = 0; i<100; i++)
        for (size_t j = 0; j<100; j++)
        {
            matrix[i][j] = 0;
        }
}

Logics::Logics()
{
    Size = 100;
    Map.push_back(Matrix());
    CurrentPosition = 0;
}


size_t Logics::countAroundRed(size_t Position, size_t index1, size_t index2)
{
    size_t Count = 0;

    if(index1 != 0 && index2 != 0)
        if (Map[Position].matrix[index1-1][index2-1] == 1) Count++;  // -1 -1
    if(index1 != 0)
        if (Map[Position].matrix[index1-1][index2] == 1)   Count++;  // -1 +0
    if(index2 != 0)
        if (Map[Position].matrix[index1][index2-1] == 1)   Count++;  // +0 -1
    if(index1 != 0 && index2 != Size-1)
        if (Map[Position].matrix[index1-1][index2+1] == 1) Count++;  // -1 +1
    if(index1 != Size-1 && index2 != 0)
        if (Map[Position].matrix[index1+1][index2-1] == 1) Count++;  // +1 -1
    if(index1 != Size-1)
        if (Map[Position].matrix[index1+1][index2] == 1)   Count++;  // +1 +0
    if(index2 != Size-1)
        if (Map[Position].matrix[index1][index2+1] == 1)   Count++;  // +0 +1
    if(index1 != Size-1 && index2 != Size-1)
        if (Map[Position].matrix[index1+1][index2+1] == 1) Count++;  // +1 +1

    return Count;
}

size_t Logics::countAroundBlue(size_t Position, size_t index1, size_t index2)
{
    size_t Count = 0;

    if(index1 != 0 && index2 != 0)
        if (Map[Position].matrix[index1-1][index2-1] == 2) Count++;  // -1 -1
    if(index1 != 0)
        if (Map[Position].matrix[index1-1][index2] == 2)   Count++;  // -1 +0
    if(index2 != 0)
        if (Map[Position].matrix[index1][index2-1] == 2)   Count++;  // +0 -1
    if(index1 != 0 && index2 != Size-1)
        if (Map[Position].matrix[index1-1][index2+1] == 2) Count++;  // -1 +1
    if(index1 != Size-1 && index2 != 0)
        if (Map[Position].matrix[index1+1][index2-1] == 2) Count++;  // +1 -1
    if(index1 != Size-1)
        if (Map[Position].matrix[index1+1][index2] == 2)   Count++;  // +1 +0
    if(index2 != Size-1)
        if (Map[Position].matrix[index1][index2+1] == 2)   Count++;  // +0 +1
    if(index1 != Size-1 && index2 != Size-1)
        if (Map[Position].matrix[index1+1][index2+1] == 2) Count++;  // +1 +1

    return Count;
}

void Logics::rulles(size_t Position, size_t index1, size_t index2)
{
    if (Map[Position-1].matrix[index1][index2] == 0)          // Правила для мертвой клетки
    {
        if (countAroundRed(Position-1, index1, index2) == 3)
            Map[Position].matrix[index1][index2] = 1;

        if (countAroundBlue(Position-1, index1, index2) == 3)
            Map[Position].matrix[index1][index2] = 2;
    }

    if (Map[Position-1].matrix[index1][index2] == 2)          // Правила для живой синей клетки
    {
        if (countAroundBlue(Position-1, index1, index2) == 2 || countAroundBlue(Position-1, index1, index2) == 3)
            Map[Position].matrix[index1][index2] = 2;
        if (countAroundBlue(Position-1, index1, index2) < 2 || countAroundBlue(Position-1, index1, index2) > 3)
            Map[Position].matrix[index1][index2] = 0;
    }

    if (Map[Position-1].matrix[index1][index2] == 1)          // Правила для живой красной клетки
    {
        if (countAroundRed(Position-1, index1, index2) == 2 || countAroundRed(Position-1, index1, index2) == 3)
            Map[Position].matrix[index1][index2] = 1;
        if (countAroundRed(Position-1, index1, index2) < 2 || countAroundRed(Position-1, index1, index2) > 3)
            Map[Position].matrix[index1][index2] = 0;
    }
}

void Logics::Next()
{
    Map.push_back(Matrix());
    for (size_t i = 0; i<100; i++)
        for (size_t j = 0; j<100; j++)
        {
            rulles(Map.size()-1, i, j);
        }
    CurrentPosition++;

    if (Map.size() > 100)
    {
        for(size_t i = 0; i<100; i++)
        {
            Map[i] = Map[i+1];
        }
        CurrentPosition--;
        Map.pop_back();
    }
}

void Logics::Previus()
{
    if (CurrentPosition != 0)
    {
        Map.pop_back();
        CurrentPosition--;
    }
}

size_t Logics::operator()(size_t i, size_t j)
{
    return Map[CurrentPosition].matrix[i][j];
}

void Logics::assignment(size_t i, size_t j, int value)
{
    Map[CurrentPosition].matrix[i][j] = value;
}
