QT += core
QT += gui
QT += core gui opengl
QT += opengl

CONFIG += c++11

TARGET = GameOfLife
CONFIG -= app_bundle
CONFIG += debug
TARGET += console

TEMPLATE = app
LIBS += -lopengl32

SOURCES += main.cpp \
    logics.cpp \
    scene.cpp \
    init.cpp

DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += \
    logics.h \
    scene.h

