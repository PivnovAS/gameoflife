#ifndef LOGICS_H
#define LOGICS_H

#include <iostream>
#include <vector>

class Logics
{
    struct Matrix
    {
        int matrix[100][100];
        Matrix();
    };

    std::vector<Matrix> Map;         // Вектор из матриц игрового поля

public:
    size_t Size;                     // Размер матрицы игрового поля
    size_t CurrentPosition;          // Текущая позиция игры

    Logics();                                        // Констуктор по умолчанию

    size_t countAround(size_t,size_t,size_t);        // Счетчик элементов вокруг
    size_t countAroundRed(size_t,size_t,size_t);     // Счетчик элементов вокруг
    size_t countAroundBlue(size_t,size_t,size_t);    // Счетчик элементов вокруг
    void rulles(size_t,size_t,size_t);               // Правила игры Жизнь
    void Next();                                     // Создание следующей позиции
    void Previus();                                  // Возвращение на предыдущую позицию

    void init();                                    // Тестовая инициализация
    void show(size_t);                              // Вывод на экран матрицы определенной позиции
    size_t operator()(size_t i, size_t j);
    void assignment(size_t i, size_t j, int value);
};

#endif // LOGICS_H
