#include "scene.h"

Scene::Scene(QWidget *parent)
    : QGLWidget(parent)
{
    Play = space_press = left_press = right_press = false;
    left_mouse = false;
    countLeftMouse = 0;
    timer = new QTimer(this);
    timer->setInterval(15);
    connect(timer,SIGNAL(timeout()),this,SLOT(update()));
    connect(timer,SIGNAL(timeout()),this,SLOT(eventHandler()));
    timer->start();
    a.init();
}

void Scene::initializeGL()
{
    glClearColor(0.0,0.0,0.0,1.0); //Задаем цвет очистки полотна
}

void Scene::resizeGL(int w, int h)
{
    glViewport(0,0,(GLsizei)w,(GLsizei)h);
    glOrtho(-1,1,-1,1,1,-1);
}


void Scene::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);


        if (Play == true) a.Next();

        for (size_t i = 0; i<100; i++)
            for(size_t j = 0; j<100; j++)
            {
                if (a(i,j) == 1)
                {
                    glColor3f(0.6,0.0,0.0);
                    GLfloat x1[] = {float(i*0.02-1+0.005), float(j*0.02-1+0.005)};
                    GLfloat x2[] = {float(i*0.02+0.02-1), float(j*0.02+0.02-1)};
                    glRectfv(x1,x2);
                }
                if (a(i,j) == 2)
                {
                    glColor3f(0.0,0.0,0.6);
                    GLfloat x1[] = {float(i*0.02-1+0.005), float(j*0.02-1+0.005)};
                    GLfloat x2[] = {float(i*0.02+0.02-1), float(j*0.02+0.02-1)};
                    glRectfv(x1,x2);
                }
            }
}

void Scene::keyPressEvent(QKeyEvent *ke)
{
    //Создаем обработчик нажатых клавиш
   if(ke->key() == Qt::Key_Space && Play == false)
       space_press = true;

   if(ke->key() == Qt::Key_Space && Play == true)
       space_press = false;

   if(ke->key() == Qt::Key_Right)
       right_press = true;

   if(ke->key() == Qt::Key_Left)
       left_press = true;
}

void Scene::keyReleaseEvent(QKeyEvent *ke)
{
    //Обработчик отпущенных клавиш
    if(ke->key() == Qt::Key_Space && Play == true)
        space_press = true;

    if(ke->key() == Qt::Key_Space && Play == false)
        space_press = false;

    if(ke->key() == Qt::Key_Right)
    {
        right_press = false;
        countRight = 0;
    }

    if(ke->key() == Qt::Key_Left)
    {
        left_press = false;
        countLeft = 0;
    }
}

void Scene::eventHandler()
{
    if(space_press) Play = true;
    if(space_press != true) Play = false;

    if(left_press && Play == false && countLeft < 1)
    {
        countLeft++;
        a.Previus();
    }

    if(right_press && Play == false && countRight < 1)
    {
        countRight++;
        a.Next();
    }

    if (left_mouse && Play == false)
    {
        if(a((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100)))) == 0 && countLeftMouse == 0)
        {
            a.assignment((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100))),1);
            countLeftMouse++;
        }

        else if(a((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100)))) == 1 && countLeftMouse == 0)
        {
            a.assignment((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100))),2);
            countLeftMouse++;
        }

        else if(a((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100)))) == 2 && countLeftMouse == 0)
        {
            a.assignment((int)((int)mouse_pos.x()/((float)width()/100)),(int)(100-((int)mouse_pos.y()-0.5)/(((float)height()/100))),0);
            countLeftMouse++;
        }
    }
}

void Scene::mousePressEvent(QMouseEvent *me)
{
    if(me->button() == Qt::LeftButton)
    {
        left_mouse = true;
        mouse_pos.setX(me->x());
        mouse_pos.setY(me->y());
    }
}

void Scene::mouseReleaseEvent(QMouseEvent *me)
{
    if(me->button() == Qt::LeftButton) left_mouse = false;
    countLeftMouse = 0;
}

Scene::~Scene()
{

}
