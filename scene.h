#ifndef SCENE_H
#define SCENE_H

#include <QOpenGLWidget>
#include <QTimer>
#include <QGLWidget>
#include <GL/gl.h>
#include <cmath>
#include <QKeyEvent>
#include "logics.h"
#include <QMouseEvent>
#include <QPoint>

class Scene : public QGLWidget
{
    Q_OBJECT
    Logics a;

public:
    Scene(QWidget *parent = 0);
    ~Scene();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void keyPressEvent(QKeyEvent* ke);
    void keyReleaseEvent(QKeyEvent* ke);
    void mousePressEvent(QMouseEvent* me);
    void mouseReleaseEvent(QMouseEvent* me);

private:
    QTimer* timer;

    bool space_press;
    bool Play;
    int countRight = 0;
    int countLeft = 0;
    bool left_press;
    bool right_press;

    bool left_mouse;
    int countLeftMouse;

    QPoint mouse_pos;

private slots:
    void eventHandler();
};

#endif // SCENE_H
